package de.rudisch.ebayTools;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import com.ebay.sdk.*;
import com.ebay.sdk.call.GetFeedbackCall;
import com.ebay.soap.eBLBaseComponents.*;
import com.ebay.sdk.helper.ConsoleUtil;

/**
 * This code describes a simple way to retrieve customer feedbacks
 * from a specific Ebay-seller using Ebay-API for Java
 * 
 * @author Dominik Rudisch
 *
 */
public class EbayFeedbackTool {

  public static void main(String[] args) {
    try {
      //Create Ebay-API Context
      ApiContext apiContext = new ApiContext();
      ApiCredential credentials = apiContext.getApiCredential();

      //Get Authentification Token
      String authToken = ConsoleUtil.readString("Please enter your eBay API Authentication Token: ");
      //Set Authentification Token
      credentials.seteBayToken(authToken);
	  
      //Set server URL
      apiContext.setApiServerUrl("https://api.ebay.com/wsapi");
      //Get seller username  
      String uID = ConsoleUtil.readString("Please enter the Ebay account name of the seller you want to get feedbacks from: ");     
      
      GetFeedbackCall feedbackCall = null;
      PaginationType p;  
      ArrayList<FeedbackDetailType[]> aLfDetail = new ArrayList<FeedbackDetailType[]>(); 
   
      //Predifine number of pages
      int numberOfPages = 1;
      
      for(int i = 0; i < numberOfPages; i++) {
    	  
    	  feedbackCall = new GetFeedbackCall(apiContext);
          feedbackCall.setUserID(uID);
          
          DetailLevelCodeType[] detailLevels = new DetailLevelCodeType[] {
                  DetailLevelCodeType.RETURN_ALL
                  
              };
          feedbackCall.setDetailLevel(detailLevels);
          
          p = new PaginationType(); 
          p.setEntriesPerPage(200);
          p.setPageNumber(i+1);
          feedbackCall.setPagination(p);
         
          //Get feedbacks on page
          aLfDetail.add(feedbackCall.getFeedback());
          
          //Redifine number of pages
          if (i == 0) numberOfPages = feedbackCall.getReturnedPaginationResult().getTotalNumberOfPages();
      }   
      
      //Check feedbacks
      if(aLfDetail.size() > 1) {
	      //Create output print writer
	      String filePath = System.getProperty("user.dir") + "\\ebay-customer-feedbacks-for-" + uID + ".txt";
	      PrintWriter printWr = new PrintWriter(new FileWriter(filePath));
	
	      printWr.println("FeedbackID;Role;ItemID;ItemTitle;ItemPrice;getCommentType;CommentTime;CommentingUser;CommentingUserScore;getCommentText");
	      
	      for (FeedbackDetailType[] fDetail : aLfDetail) {
		      for (FeedbackDetailType f : fDetail) {
		    	  try {
		    		  if(f.getRole() == TradingRoleCodeType.SELLER) {
				    	  printWr.println( f.getRole() + ";" + f.getItemID() + ";" + f.getItemTitle()  + ";" + f.getCommentType() + ";" +  f.getCommentingUser() + ";" + f.getCommentText());
		    		  }
		    	  } catch (Exception exep) {
		    		  exep.printStackTrace();
		    	  }
		      }
	      }
	      printWr.close();
	      System.out.println("Successfully retieved " + feedbackCall.getReturnedPaginationResult().getTotalNumberOfEntries() + " feedbacks on " + numberOfPages + " pages!");
	      System.out.println("Retrieved seller feedbacks were saved in file: \"" + filePath + "\"");
      }
    }
    catch(Exception e) {
    }
  }
}
